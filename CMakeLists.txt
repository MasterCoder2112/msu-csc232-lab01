cmake_minimum_required(VERSION 3.6)
project(Lab01)

set(CMAKE_CXX_STANDARD 14)

set(SRC_RELEASE_FILES src/release/Main.cpp src/release/Circle.cpp)
set(SRC_DEBUG_FILES src/debug/LabTestRunner.cpp src/release/Circle.cpp src/debug/CircleTest.cpp)

add_executable(Lab01 ${SRC_RELEASE_FILES})
add_executable(Lab01_Test ${SRC_DEBUG_FILES})

target_link_libraries(Lab01_Test PUBLIC -lcppunit)
