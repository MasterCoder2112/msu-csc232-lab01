/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Shape.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <fillmein@missouristate.edu>
 *          Partner Name <partnerid@missouristate.edu>
 * @brief   Abstract base class of Shape class hierarchy.
 */

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {
  
  /*
   * Modern replacement for typedef statement used as an alias for doubles 
   * in csc232 namespace 
   */
  using length_unit = double;

  /**
   * @brief Abstract base class, i.e., interface, for Shape class hierarchy.
   */
  class Shape {
  public:
    /**
     * Get the perimeter of this Shape.
     *
     * @return The perimeter of this Shape is returned.
     * @post   The state of this Shape is unchanged by this operation.
     */
    virtual length_unit getPerimeter() const = 0;

    /**
     * Get the area of this Shape.
     * 
     * @return The area of this Shape is returned.
     * @post   The state of this Shape is unchanged by this operation.
     */
    virtual length_unit getArea() const = 0;
  };
}
