/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Circle.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Alexander James Byrd <zz2112@live.missouristate.edu>
 *          Andrew Waszak <adw423@live.missouristate.edu>
 *          Moises Suazo <Moises123@live.missouristate.edu>
 * @brief   Circle implemention.
 */

#include "Circle.h"

namespace csc232 {

  /* Constructors */
  Circle::Circle() : m_radius(1.0) {
    // no-op
  }

  // TODO: modify initializer to prevent storing radius <= 0.
  Circle::Circle(const double radius) : m_radius(radius <= 0 ? 1.0: radius) {
    // no-op
  }

  /* Getter/Setters */
 /**
  * Returns radius stored of the circle
  */
  double Circle::getRadius() const {

    return m_radius;
  }
  
 /**
  * Sets the radius of the circle, making sure it is not less than or
  * equal to 0, and if so set it to the default of 1
  */
  void Circle::setRadius(const double radius) {
	  radius <= 0 ? m_radius = m_radius : m_radius = radius;
  }

  /* Overrides */
 /**
  * Sends back the perimeter of the circle
  */
  double Circle::getPerimeter() const {
    
    return 2 * 3.1415926535 * m_radius;
  }
  
 /**
  * Sends back area of the circle
  */
  double Circle::getArea() const {
    
    return 3.1415926535 * m_radius * m_radius;
  }

  /* Additional operations */
 /**
  * Sends back Circumference of the circle
  */
  double Circle::getCircumference() const {
    
    return getPerimeter();
  }

}
