/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <fillmein@missouristate.edu>
 *          Partner Name <partnerid@missouristate.edu>
 * @brief   Entry point for Lab 1 release demo.
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include "Circle.h"

/**
 * @brief Entry point for Lab 1 release demo.
 * @param argc the number of command line arguments, including the executable
 * @param argv an array of the command line arguments, including the executable
 * @return EXIT_SUCCESS is returned upon successful execution.
 */
int main(int argc, char* argv[]) {
  csc232::Circle unitCircle;
  std::cout << "unitCircle.getRadius() = " << std::fixed << std::setprecision(2)
	    << unitCircle.getRadius() << std::endl;

  return EXIT_SUCCESS;
}
