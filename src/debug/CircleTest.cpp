/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    CircleTest.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <fillmein@missouristate.edu>
 *          Partner Name <partnerid@missouristate.edu>
 * @brief   Circle CPPUNIT test implementation.
 * @see     http://sourceforge.net/projects/cppunit/files
 */

#define _USE_MATH_DEFINES

#include <iomanip>
#include <sstream>
#include "CircleTest.h"

CPPUNIT_TEST_SUITE_REGISTRATION(CircleTest);

std::string getMessage(const std::string &attribute, const double expected,
		       const double actual) {
  std::stringstream ss;
  ss << std::fixed << std::setprecision(CircleTest::MSG_PRECISION)
     << "This test failed because:\n\t"
     << "expected " << attribute << "(" << expected
     << ") does not equal actual " << attribute << " ("
     << actual << ")";
  return ss.str();
}

CircleTest::CircleTest() {
}

CircleTest::~CircleTest() {
}

void CircleTest::setUp() {
}

void CircleTest::tearDown() {
}

void CircleTest::defaultCircleIsUnitCircle() {
  const double expectedRadius{UNIT_CIRCLE_RADIUS};
  const double actualRadius = unitCircle.getRadius();
  std::string message{getMessage("radius", expectedRadius, actualRadius)};
  
  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expectedRadius, actualRadius,
				       DELTA);
}

void CircleTest::initializedCircleHasExpectedRadius() {
  const double expectedRadius{INIT_CIRCLE_RADIUS};
  const double actualRadius = initializedCircle.getRadius();
  std::string message{getMessage("radius", expectedRadius, actualRadius)};
  
  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expectedRadius, actualRadius, DELTA);
}


void CircleTest::defaultCircleHasExpectedPerimeter() {
  const double expectedPerimeter{UNIT_CIRCLE_PERIM};
  const double actualPerimeter{unitCircle.getPerimeter()};
  std::string message{getMessage("perimeter", expectedPerimeter, actualPerimeter)};
  
  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expectedPerimeter, actualPerimeter, DELTA);
}

void CircleTest::initializedCircleHasExpectedPerimeter() {
  const double expectedPerimeter{INIT_CIRCLE_PERIM};
  const double actualPerimeter{initializedCircle.getPerimeter()};
  std::string message{getMessage("perimeter", expectedPerimeter, actualPerimeter)};
  
  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expectedPerimeter, actualPerimeter, DELTA);
}

void CircleTest::defaultCircleHasExpectedArea() {
  const double expectedArea{UNIT_CIRCLE_AREA};
  const double actualArea{unitCircle.getArea()};
  std::string message{getMessage("area", expectedArea, actualArea)};
  
  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expectedArea, actualArea, DELTA);
}

void CircleTest::initializedCircleHasExpectedArea() {
  const double expectedArea{INIT_CIRCLE_AREA};
  const double actualArea{initializedCircle.getArea()};
  std::string message{getMessage("area", expectedArea, actualArea)};
  
  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expectedArea, actualArea, DELTA);
}

void CircleTest::defaultCircleHasExpectedCircumference() {
  const double expectedCircumference{UNIT_CIRCLE_PERIM};
  const double actualCircumference{unitCircle.getCircumference()};
  std::string message{getMessage("circumference", expectedCircumference, actualCircumference)};

  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expectedCircumference, actualCircumference, DELTA);
}

void CircleTest::initializedCircleHasExpectedCircumference() {
  const double expectedCircumference{INIT_CIRCLE_PERIM};
  const double actualCircumference{initializedCircle.getCircumference()};
  std::string message{getMessage("circumference", expectedCircumference, actualCircumference)};

  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expectedCircumference, actualCircumference, DELTA);
}

void CircleTest::setRadiusChangesRadiusAsExpected() {
  initializedCircle.setRadius(PI);
  const double expectedRadius{PI};
  const double actualRadius{initializedCircle.getRadius()};
  std::string message{getMessage("radius", expectedRadius, actualRadius)};

  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(message, expectedRadius, actualRadius, DELTA);
}
